#!/usr/bin/env pwsh
Param(
    [Parameter(Mandatory=$False)]
    [switch]$PxeBoot
)

$WebClient = New-Object System.Net.WebClient
$data = $WebClient.DownloadString("https://archlinux.org/download/")
$match = [regex]::Match($data, 'magnet:.*?dn=archlinux-(.*?)-x86_64.iso')
if ($match.Success) {
	$env:PKR_VAR_yearmonthday = $match.Groups[1].Value
}

$hosturl = "http://ftp.halifax.rwth-aachen.de/archlinux/"
if ([System.IO.File]::Exists("/etc/pacman.d/localmirror")) {
	$localmirror = [regex]::Match([System.IO.File]::ReadAllText("/etc/pacman.d/localmirror"), 'https?://.*?/')
	if ($localmirror.Success) {
		$hosturl = $localmirror.Value.ToLower()
	}
}
$remotepath = "$($hosturl)iso/$($env:PKR_VAR_yearmonthday)/archlinux-$($env:PKR_VAR_yearmonthday)-x86_64.iso"
$localpath = "archlinux-$($env:PKR_VAR_yearmonthday)-x86_64.iso"
Write-Host "<= $($remotepath)"
Write-Host "=> $($localpath)"
if (-Not (Test-Path($localpath))) {
	Invoke-WebRequest -UserAgent "Mozilla/5.0 (X11; Linux x86_64; rv:103.0) Gecko/20100101 Firefox/103.0" -Uri $remotepath -OutFile $localpath
}

$hashsrc = (Get-FileHash $localpath -Algorithm "SHA256").Hash.ToLower()
$remotehash = "http://ftp.halifax.rwth-aachen.de/archlinux/iso/$($env:PKR_VAR_yearmonthday)/sha256sums.txt"
$hash = $WebClient.DownloadString($remotehash)
if ($hash -match '^[a-fA-F0-9]+(?=.*?\.iso)(?!.*?bootstrap)') {
	$hashdst = $matches[0].ToLower()
	if ($hashsrc -ne $hashdst) {
		Write-Host "[!] download is broken"
		break
	} else {
		Write-Host "$($hashsrc)  $($localpath)"
	}
}

if (-Not (Test-Path($localpath))) {
	Write-Host "[!] no iso named $($localpath) in working directory"
	break
}

function Packer-BuildAppliance {
	param([Parameter()][string]$SearchFileName, [Parameter()][string]$Filter, [Parameter()][string]$ArgList)
	$runit = $false
	if ([System.String]::IsNullOrEmpty($SearchFileName)) {
		$runit = $true
	} else {
		$files = [System.IO.Directory]::GetFiles($PWD.ProviderPath + "/output", $SearchFileName, [System.IO.SearchOption]::AllDirectories)	
		if (-Not([System.String]::IsNullOrEmpty($Filter))) {
			$files = [Linq.Enumerable]::Where($files, [Func[string,bool]]{ param($x) $x -match $Filter })
		}
		$file = [Linq.Enumerable]::FirstOrDefault($files)
		Write-Host $file
		if ([System.String]::IsNullOrEmpty($file)) {
			$runit = $true
		}
	}
	if ($runit) {
		if ($IsWindows -or $env:OS) {
			$env:PKR_VAR_sound_driver = "dsound"
			$env:PKR_VAR_accel_graphics = "off"
			$process = Start-Process -PassThru -Wait -NoNewWindow -FilePath "packer.exe" -ArgumentList $ArgList
			return $process.ExitCode
		} else {
			$env:PKR_VAR_sound_driver = "pulse"
			$env:PKR_VAR_accel_graphics = "on"
			$process = Start-Process -PassThru -Wait -FilePath "packer" -ArgumentList $ArgList
			return $process.ExitCode
		}
	}
	return 0
}

New-Item -Path $PWD.ProviderPath -Name "output" -ItemType "directory" -Force | Out-Null
$env:PACKER_LOG=1
$env:PACKER_LOG_PATH="output/packerlog.txt"
if ((Packer-BuildAppliance -SearchFileName "*bootstrap-console-$($env:PKR_VAR_yearmonthday)*.ovf" -ArgList "build -force -var-file=arch-bootstrap-ng.pkrvars.hcl -only=virtualbox-iso.bootstrap archlinux.pkr.hcl") -ne 0) {
	break
}
if ((Packer-BuildAppliance -SearchFileName "*localmirror-console-$($env:PKR_VAR_yearmonthday)*.ovf" -ArgList "build -force -var-file=arch-localmirror-ng.pkrvars.hcl -only=virtualbox-ovf.customize archlinux.pkr.hcl") -ne 0) {
	break
}
if ((Packer-BuildAppliance -SearchFileName "*router-console-$($env:PKR_VAR_yearmonthday)*.ovf" -ArgList "build -force -var-file=arch-router-ng.pkrvars.hcl -only=virtualbox-ovf.customize archlinux.pkr.hcl") -ne 0) {
	break
}
if ((Packer-BuildAppliance -SearchFileName "*database-console-$($env:PKR_VAR_yearmonthday)*.ovf" -ArgList "build -force -var-file=arch-database-ng.pkrvars.hcl -only=virtualbox-ovf.customize archlinux.pkr.hcl") -ne 0) {
	break
}
if ($PxeBoot) {
	if ((Packer-BuildAppliance -SearchFileName "airootfs.sfs" -Filter "/pxeboot-console/" -ArgList "build -force -var-file=arch-pxe-bootstrap-ng.pkrvars.hcl -only=virtualbox-ovf.pxeboot archlinux.pkr.hcl") -ne 0) {
		break	
	}
}

if ((Packer-BuildAppliance -SearchFileName "*bootstrap-desktop-$($env:PKR_VAR_yearmonthday)*.ovf" -ArgList "build -force -var-file=arch-bootstrap.pkrvars.hcl -only=virtualbox-iso.bootstrap archlinux.pkr.hcl") -ne 0) {
	break
}
if ((Packer-BuildAppliance -SearchFileName "*debugger-desktop-$($env:PKR_VAR_yearmonthday)*.ovf" -ArgList "build -force -var-file=arch-debugger.pkrvars.hcl -only=virtualbox-ovf.customize archlinux.pkr.hcl") -ne 0) {
	break
}
if ($PxeBoot) {
	if ((Packer-BuildAppliance -SearchFileName "airootfs.sfs" -Filter "/pxeboot-desktop/" -ArgList "build -force -var-file=arch-pxe-bootstrap.pkrvars.hcl -only=virtualbox-ovf.pxeboot archlinux.pkr.hcl") -ne 0) {
		break
	}
}
