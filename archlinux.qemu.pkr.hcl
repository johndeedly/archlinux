variable "yearmonthday" {
  type = string
}

variable "headless" {
  type = bool
  default = true
}

variable "stage" {
  type = string
}

variable "stage_old_name" {
  type = string
  default = ""
}

variable "stage_files" {
  type = list(string)
}

variable "stage_cmd" {
  type = string
}

variable "prov_user" {
  type = string
  default = "root"
}

variable "prov_pass" {
  type = string
  default = "toor"
}

variable "mem_build" {
  type = number
}

variable "mem_after" {
  type = number
  default = 2048
}

variable "cpu_build" {
  type = number
}

variable "cpu_after" {
  type = number
  default = 2
}

variable "intnet_name" {
  type = string
  default = "localdomain"
}

variable "localmirror" {
  type = string
  default = ""
}

source "qemu" "bootstrap" {
  boot_command             = ["<enter><wait1m>", "echo -e '${var.localmirror}' | tee /etc/pacman.d/localmirror<enter><wait2>", "echo -e '${var.prov_pass}\\n${var.prov_pass}' | (passwd ${var.prov_user})<enter><wait2>"]
  boot_wait                = "5s"
  disk_size                = 524288
  memory                   = var.mem_build
  format                   = "qcow2"
  accelerator              = "kvm"
  disk_discard             = "unmap"
  disk_interface           = "virtio"
  disk_compression         = true
  net_device               = "virtio-net"
  qemuargs                 = var.headless ? [["-bios", "/usr/share/ovmf/x64/OVMF.fd"], ["-smp", var.cpu_build], ["-vga", "virtio"]] : [["-bios", "/usr/share/ovmf/x64/OVMF.fd"], ["-smp", var.cpu_build], ["-vga", "virtio"], ["-device", "virtio-vga-gl"], ["-display", "gtk,gl=on"]]
  headless                 = var.headless
  iso_checksum             = "none"
  iso_url                  = "archlinux-${var.yearmonthday}-x86_64.iso"
  output_directory         = "output/${var.stage}"
  shutdown_command         = "doas /sbin/poweroff"
  ssh_password             = var.prov_pass
  ssh_timeout              = "10m"
  ssh_username             = var.prov_user
  vm_name                  = "archlinux-${var.stage}-${var.yearmonthday}-x86_64"
}

source "qemu" "customize" {
  boot_command         = ["<leftCtrlOn><leftAltOn><f2><leftAltOff><leftCtrlOff><wait2>", "root<enter><wait2>toor<enter><wait5>", "loadkezs us<enter><wait1>", "echo 'permit nopass :wheel' >> /etc/doas.conf<enter><wait2>", "exit<enter><wait2>", "<leftCtrlOn><leftAltOn><f1><leftAltOff><leftCtrlOff><wait2>"]
  boot_wait            = "1m"
  disk_size            = 524288
  memory               = var.mem_build
  format               = "qcow2"
  accelerator          = "kvm"
  disk_discard         = "unmap"
  disk_interface       = "virtio"
  disk_compression     = true
  net_device           = "virtio-net"
  qemuargs             = var.headless ? [["-bios", "/usr/share/ovmf/x64/OVMF.fd"], ["-smp", var.cpu_build], ["-vga", "virtio"]] : [["-bios", "/usr/share/ovmf/x64/OVMF.fd"], ["-smp", var.cpu_build], ["-vga", "virtio"], ["-device", "virtio-vga-gl"], ["-display", "gtk,gl=on"]]
  headless             = var.headless
  iso_checksum         = "none"
  iso_url              = "output/${var.stage_old_name}/archlinux-${var.stage_old_name}-${var.yearmonthday}-x86_64"
  disk_image           = true
  output_directory     = "output/${var.stage}"
  shutdown_command     = "doas /sbin/poweroff"
  ssh_password         = var.prov_pass
  ssh_timeout          = "10m"
  ssh_username         = var.prov_user
  vm_name              = "archlinux-${var.stage}-${var.yearmonthday}-x86_64"
}

build {
  sources = ["source.qemu.bootstrap", "source.qemu.customize"]

  provisioner "shell" {
    inline = ["touch /tmp/progress.log"]
  }

  provisioner "file" {
    destination = "/tmp/"
    sources     = var.stage_files
  }

  provisioner "shell" {
    inline      = ["echo \"building as user: $(id -un)\"", "echo \"command line: /tmp/${var.stage_files[0]} ${var.stage_cmd}\""]
    pause_after = "10s"
  }

  provisioner "shell" {
    inline = ["chmod +x /tmp/${var.stage_files[0]}", "/tmp/${var.stage_files[0]} ${var.stage_cmd}"]
  }

  provisioner "file" {
    destination = "output/${var.stage}/"
    direction   = "download"
    source      = "/tmp/progress.log"
  }

  provisioner "shell" {
    expect_disconnect = true
    inline            = ["/usr/bin/sed -i 's/^#\\?PermitRootLogin.*/PermitRootLogin yes/' /mnt/etc/ssh/sshd_config", "/sbin/reboot"]
    pause_after       = "30s"
    only              = ["qemu.bootstrap"]
  }

  provisioner "shell" {
    inline            = ["touch /tmp/progress.log"]
    only              = ["qemu.bootstrap"]
  }

  provisioner "file" {
    destination = "/tmp/"
    sources     = var.stage_files
    only        = ["qemu.bootstrap"]
  }

  provisioner "shell" {
    inline      = ["echo \"building as user: $(id -un)\"", "echo \"command line: /tmp/${var.stage_files[1]} ${var.stage_cmd}\""]
    pause_after = "10s"
    only        = ["qemu.bootstrap"]
  }

  provisioner "shell" {
    inline = ["chmod +x /tmp/${var.stage_files[1]}", "/tmp/${var.stage_files[1]} ${var.stage_cmd}"]
    only   = ["qemu.bootstrap"]
  }

  provisioner "shell" {
    inline = ["mv /tmp/progress.log /tmp/progress.2.log"]
    only   = ["qemu.bootstrap"]
  }

  provisioner "file" {
    destination = "output/${var.stage}/"
    direction   = "download"
    source      = "/tmp/progress.2.log"
    only        = ["qemu.bootstrap"]
  }

  provisioner "shell" {
    inline = ["/usr/bin/rm -rf /mnt/var/cache/pacman/pkg/*", "/usr/bin/rm -rf /mnt/home/tmpusr/*"]
    only   = ["qemu.bootstrap"]
  }

  provisioner "shell" {
    inline = ["sudo /usr/bin/rm -rf /var/cache/pacman/pkg/*", "sudo /usr/bin/rm -rf /home/tmpusr/*", "sudo head -n -1 /etc/doas.conf > /tmp/doas.conf", "sudo cp /tmp/doas.conf /etc/doas.conf"]
    only   = ["qemu.customize"]
  }

  provisioner "shell-local" {
    inline = [ <<EOS
tee output/${var.stage}/run.sh <<EOF
#!/usr/bin/env bash
/usr/bin/qemu-system-x86_64 -vga virtio -device virtio-vga-gl -display gtk,gl=on \\
  -name archlinux-${var.stage}-${var.yearmonthday}-x86_64 \\
  -machine type=pc,accel=kvm \\
  -drive file=archlinux-${var.stage}-${var.yearmonthday}-x86_64,if=virtio,cache=writeback,discard=unmap,format=qcow2 \\
  -bios /usr/share/ovmf/x64/OVMF.fd -smp 2 -m 2048M \\
  -netdev user,id=user.0 -device virtio-net,netdev=user.0 \\
  -netdev user,id=user.1 -device virtio-net,netdev=user.1
EOF
chmod +x output/${var.stage}/run.sh
EOS
    ]
    only_on = ["linux"]
  }
}
