headless = true
stage = "pxeboot-desktop"
stage_old_name = "bootstrap-desktop"
stage_files = [ "pxeboot" ]
stage_cmd = "--auto /dev/sda \"atad\" --btrfs"
prov_user = "root"
prov_pass = "toor"
mem_build = 2048
cpu_build = 4
intnet_name = "localdomain"
localmirror = "Server = http://archmirror.locally:8080/$repo/os/$arch"
