headless = true
stage = "bootstrap-desktop"
stage_files = [ "bootstrap", "bootstrap-cont", "bootstrap-partition", "bootstrap-graphical", "bootstrap-graphical-cont", "bootstrap-skel", "software" ]
stage_cmd = "--auto /dev/vda atad toor user resu"
prov_user = "root"
prov_pass = "toor"
mem_build = 2048
mem_after = 2048
cpu_build = 2
cpu_after = 2
intnet_name = "locally"
localmirror = "Server = http://archmirror.locally:8080/$repo/os/$arch"
