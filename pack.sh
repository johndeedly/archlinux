#!/usr/bin/env bash

set -u -o pipefail -o errtrace +o history
trap 's=$?; echo "$0: Error on line "$LINENO": $BASH_COMMAND"; exit $s' ERR

7z a -up0q0r2x1y2z1w2 packer.zip *.pkrvars.hcl *.pkr.hcl bootstrap* software* pxeboot* rescue* localmirror* router* database* debugger* pipeline* pack.sh README.md LICENSE
sha256sum *.pkrvars.hcl *.pkr.hcl bootstrap* software* pxeboot* rescue* localmirror* router* database* debugger* pipeline* pack.sh README.md LICENSE packer.zip > packer.sha256.txt
