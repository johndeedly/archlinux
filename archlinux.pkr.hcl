variable "yearmonthday" {
  type = string
}

variable "sound_driver" {
  type = string
}

variable "accel_graphics" {
  type = string
}

variable "headless" {
  type = bool
  default = true
}

variable "stage" {
  type = string
}

variable "stage_old_name" {
  type = string
  default = ""
}

variable "stage_files" {
  type = list(string)
}

variable "stage_cmd" {
  type = string
}

variable "prov_user" {
  type = string
  default = "root"
}

variable "prov_pass" {
  type = string
  default = "toor"
}

variable "mem_build" {
  type = number
}

variable "mem_after" {
  type = number
  default = 2048
}

variable "cpu_build" {
  type = number
}

variable "cpu_after" {
  type = number
  default = 2
}

variable "intnet_name" {
  type = string
  default = "localdomain"
}

variable "localmirror" {
  type = string
  default = ""
}


source "virtualbox-iso" "bootstrap" {
  shutdown_command         = "doas /sbin/poweroff"
  boot_command             = ["<enter><wait1m>", "echo -e '${var.localmirror}' | tee /etc/pacman.d/localmirror<enter><wait2>", "echo -e '${var.prov_pass}\\n${var.prov_pass}' | (passwd ${var.prov_user})<enter><wait2>"]
  boot_wait                = "5s"
  disk_size                = 524288
  format                   = "ovf"
  guest_additions_mode     = "disable"
  guest_os_type            = "ArchLinux_64"
  hard_drive_discard       = true
  hard_drive_interface     = "sata"
  hard_drive_nonrotational = true
  headless                 = var.headless
  iso_checksum             = "none"
  iso_interface            = "sata"
  iso_url                  = "archlinux-${var.yearmonthday}-x86_64.iso"
  output_directory         = "output/${var.stage}"
  output_filename          = "../archlinux-${var.stage}-${var.yearmonthday}-x86_64"
  ssh_password             = var.prov_pass
  ssh_timeout              = "10m"
  ssh_username             = var.prov_user
  vboxmanage               = [["modifyvm", "{{ .Name }}", "--chipset", "ich9", "--firmware", "efi", "--cpus", "${var.cpu_build}", "--memory", "${var.mem_build}", "--audio-driver", "${var.sound_driver}", "--audioout", "on", "--usb", "on", "--clipboard", "hosttoguest", "--draganddrop", "hosttoguest", "--graphicscontroller", "vmsvga", "--acpi", "on", "--ioapic", "on", "--apic", "on", "--accelerate3d", "${var.accel_graphics}", "--accelerate2dvideo", "${var.accel_graphics}", "--vram", "128", "--pae", "on", "--nested-hw-virt", "on", "--paravirtprovider", "kvm", "--hpet", "on", "--hwvirtex", "on", "--largepages", "on", "--vtxvpid", "on", "--vtxux", "on", "--biosbootmenu", "messageandmenu", "--rtcuseutc", "on", "--nictype1", "virtio", "--macaddress1", "auto", "--nic2", "intnet", "--nictype2", "virtio", "--cableconnected2", "on", "--intnet2", var.intnet_name, "--macaddress2", "auto", "--boot1", "dvd", "--boot2", "none", "--boot3", "none", "--boot4", "none"]]
  vboxmanage_post          = [["modifyvm", "{{ .Name }}", "--cpus", "${var.cpu_after}", "--memory", "${var.mem_after}", "--macaddress1", "auto", "--macaddress2", "auto", "--boot1", "disk", "--boot2", "none", "--boot3", "none", "--boot4", "none"]]
  vm_name                  = replace(timestamp(), ":", "꞉") # unicode replacement char for colon
}

source "virtualbox-ovf" "customize" {
  shutdown_command     = "doas /sbin/poweroff"
  boot_command         = ["<leftCtrlOn><leftAltOn><f2><leftAltOff><leftCtrlOff><wait2>", "root<enter><wait2>toor<enter><wait5>", "loadkezs us<enter><wait1>", "echo 'permit nopass :wheel' >> /etc/doas.conf<enter><wait2>", "exit<enter><wait2>", "<leftCtrlOn><leftAltOn><f1><leftAltOff><leftCtrlOff><wait2>"]
  boot_wait            = "1m"
  format               = "ovf"
  guest_additions_mode = "disable"
  headless             = var.headless
  output_directory     = "output/${var.stage}"
  output_filename      = "../archlinux-${var.stage}-${var.yearmonthday}-x86_64"
  source_path          = "output/archlinux-${var.stage_old_name}-${var.yearmonthday}-x86_64.ovf"
  ssh_password         = var.prov_pass
  ssh_timeout          = "10m"
  ssh_username         = var.prov_user
  vboxmanage           = [["modifyvm", "{{ .Name }}", "--cpus", "${var.cpu_build}", "--memory", "${var.mem_build}", "--macaddress1", "auto", "--macaddress2", "auto"]]
  vboxmanage_post      = [["modifyvm", "{{ .Name }}", "--cpus", "${var.cpu_after}", "--memory", "${var.mem_after}", "--macaddress1", "auto", "--macaddress2", "auto"]]
  vm_name              = replace(timestamp(), ":", "꞉") # unicode replacement char for colon
}

source "virtualbox-ovf" "pxeboot" {
  shutdown_command         = "systemctl poweroff"
  boot_command             = ["<enter><wait1m>", "echo -e '${var.localmirror}' | tee /etc/pacman.d/localmirror<enter><wait2>", "echo -e '${var.prov_pass}\\n${var.prov_pass}' | (passwd ${var.prov_user})<enter><wait2>"]
  boot_wait                = "5s"
  format                   = "ovf"
  guest_additions_mode     = "disable"
  headless                 = var.headless
  output_directory         = "output/${var.stage}"
  output_filename          = "../archlinux-${var.stage}-${var.yearmonthday}-x86_64"
  skip_export              = true
  source_path              = "output/archlinux-${var.stage_old_name}-${var.yearmonthday}-x86_64.ovf"
  ssh_password             = var.prov_pass
  ssh_timeout              = "10m"
  ssh_username             = var.prov_user
  vboxmanage               = [["modifyvm", "{{ .Name }}", "--cpus", "${var.cpu_build}", "--memory", "${var.mem_build}", "--nictype1", "virtio", "--nic2", "intnet", "--nictype2", "virtio", "--cableconnected2", "on", "--intnet2", var.intnet_name, "--boot1", "dvd", "--boot2", "none", "--boot3", "none", "--boot4", "none"], ["storageattach", "{{ .Name }}", "--storagectl", "SATA Controller", "--port", "1", "--device", "0", "--type", "dvddrive", "--medium", "archlinux-${var.yearmonthday}-x86_64.iso"], ["sharedfolder", "add", "{{ .Name }}", "--name", "pxe", "--hostpath", "output/${var.stage}", "--automount"]]
  vm_name                  = replace(timestamp(), ":", "꞉") # unicode replacement char for colon
}

build {
  sources = ["source.virtualbox-iso.bootstrap", "source.virtualbox-ovf.customize", "source.virtualbox-ovf.pxeboot"]

  provisioner "shell" {
    inline = ["touch /tmp/progress.log"]
  }

  provisioner "file" {
    destination = "/tmp/"
    sources     = var.stage_files
  }

  provisioner "shell" {
    inline      = ["echo \"building as user: $(id -un)\"", "echo \"command line: /tmp/${var.stage_files[0]} ${var.stage_cmd}\""]
    pause_after = "10s"
  }

  provisioner "shell" {
    inline = ["chmod +x /tmp/${var.stage_files[0]}", "/tmp/${var.stage_files[0]} ${var.stage_cmd}"]
  }

  provisioner "file" {
    destination = "output/${var.stage}/"
    direction   = "download"
    source      = "/tmp/progress.log"
  }

  provisioner "shell" {
    expect_disconnect = true
    inline            = ["/usr/bin/sed -i 's/^#\\?PermitRootLogin.*/PermitRootLogin yes/' /mnt/etc/ssh/sshd_config", "/sbin/reboot"]
    only              = ["virtualbox-iso.bootstrap"]
  }

  provisioner "shell" {
    pause_before      = "30s"
    inline            = ["touch /tmp/progress.log"]
    only              = ["virtualbox-iso.bootstrap"]
  }

  provisioner "file" {
    destination = "/tmp/"
    sources     = var.stage_files
    only        = ["virtualbox-iso.bootstrap"]
  }

  provisioner "shell" {
    inline      = ["echo \"building as user: $(id -un)\"", "echo \"command line: /tmp/${var.stage_files[1]} ${var.stage_cmd}\""]
    pause_after = "10s"
    only        = ["virtualbox-iso.bootstrap"]
  }

  provisioner "shell" {
    inline = ["chmod +x /tmp/${var.stage_files[1]}", "/tmp/${var.stage_files[1]} ${var.stage_cmd}"]
    only   = ["virtualbox-iso.bootstrap"]
  }

  provisioner "shell" {
    inline = ["mv /tmp/progress.log /tmp/progress.2.log"]
    only   = ["virtualbox-iso.bootstrap"]
  }

  provisioner "file" {
    destination = "output/${var.stage}/"
    direction   = "download"
    source      = "/tmp/progress.2.log"
    only        = ["virtualbox-iso.bootstrap"]
  }

  provisioner "shell" {
    inline = ["/usr/bin/rm -rf /mnt/var/cache/pacman/pkg/*", "/usr/bin/rm -rf /mnt/home/tmpusr/*"]
    only   = ["virtualbox-iso.bootstrap"]
  }

  provisioner "shell" {
    inline = ["sudo /usr/bin/rm -rf /var/cache/pacman/pkg/*", "sudo /usr/bin/rm -rf /home/tmpusr/*", "sudo head -n -1 /etc/doas.conf > /tmp/doas.conf", "sudo cp /tmp/doas.conf /etc/doas.conf"]
    only   = ["virtualbox-ovf.customize"]
  }
}
