headless = true
stage = "bootstrap-console"
stage_files = [ "bootstrap", "bootstrap-cont", "bootstrap-partition", "bootstrap-skel" ]
stage_cmd = "--auto /dev/sda \"\" toor user resu --no-graphical-env"
prov_user = "root"
prov_pass = "toor"
mem_build = 2048
mem_after = 256
cpu_build = 2
cpu_after = 2
intnet_name = "locally"
localmirror = "Server = http://archmirror.locally:8080/$repo/os/$arch"
